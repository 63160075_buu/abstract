/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.shapeabstract;

/**
 *
 * @author ACER
 */
public class Square extends Shape{
    private double x;
    
    public Square(double x){
    super("Square");
    this.x=x;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }
    
    @Override
     public double calArea(){
          return x*x;
      }
     
    @Override
       public String toString(){
        return "Square{" + "side = " + x + '}';
    }
}
