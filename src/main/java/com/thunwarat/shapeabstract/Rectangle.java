/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.shapeabstract;

/**
 *
 * @author ACER
 */
public class Rectangle extends Shape{
    
    private double x;
    private double y;
    
    public Rectangle(double x, double y) {
        super("Rectangle");
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }
    
    @Override
    public double calArea(){
          return x*y;
      }
    
    @Override
    public String toString(){
       return "Rectangle{" + "width = " + x + ", hight = " + y + '}'; 
    }
}
