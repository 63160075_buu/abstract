/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.shapeabstract;

/**
 *
 * @author ACER
 */
public class TestShape {
     public static void main(String[] args) {
        Circle circle = new Circle(10); 
        System.out.println(circle);
        Rectangle rectangle = new Rectangle(5,7);
        System.out.println(rectangle);
        Square square = new Square(20);
        System.out.println(square);
        Triangle triangle = new Triangle(6, 14);
        System.out.println(triangle);
        
        Shape[] shapes = {circle, rectangle, square, triangle};
            for(int i = 0; i < shapes.length; i++){
                System.out.print(shapes[i].getName() + " Area : " + shapes[i].calArea());
            }
     }
}
